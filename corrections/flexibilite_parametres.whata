[set
    title       = "Nombre variable de paramètres"
    partAs      = correction
    translator  = "Olivier Pisano"
    proofreader = "Stéphane Goujet"
]

Pour que la fonction ``calculer`` puisse accepter un nombre variable de paramètres, elle doit inclure une tranche de ``Calcul`` suivie de ``...``~ :

[code=d <<<
    double[] calculer(in Calcul[] calculs ...)
    {
        double[] resultats;

        foreach (calcul; calculs) {
            final switch (calcul.op) {
                case Operation.addition:
                    resultats ~= calcul.premier + calcul.second;
                    break;

                case Operation.soustraction:
                    resultats ~= calcul.premier – calcul.second;
                    break;

                case Operation.multiplication:
                    resultats ~= calcul.premier * calcul.second;
                    break;

                case Operation.division:
                    resultats ~= calcul.premier /  calcul.second;
                    break;
            }
        }

        return resultats;
}
>>>]

Voici le programme complet~ :

[code=d <<<
    import std.stdio;

    enum Operation { addition, soustraction, multiplication, division }

    struct Calcul {
        Operation op;
        double premier;
        double second;
    }

    double[] calculer(in Calcul[] calculs ...)
    {
        double[] resultats;

        foreach (calcul; calculs) {
            final switch (calcul.op) {
                case Operation.addition:
                    resultats ~= calcul.premier + calcul.second;
                    break;

                case Operation.soustraction:
                    resultats ~= calcul.premier - calcul.second;
                    break;

                case Operation.multiplication:
                    resultats ~= calcul.premier * calcul.second;
                    break;

                case Operation.division:
                    resultats ~= calcul.premier /  calcul.second;
                    break;
            }
        }

        return resultats;
    }

    void main() {
        writeln(calculer(Calcul(Operation.addition, 1.1, 2.2),
                         Calcul(Operation.soustraction, 3.3, 4.4),
                         Calcul(Operation.multiplication, 5.5, 6.6),
                         Calcul(Operation.division, 7.7, 8.8)));
    }
>>>]
